package com.li.model.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.arcsoft.face.*;
import com.arcsoft.face.enums.DetectMode;
import com.arcsoft.face.enums.DetectOrient;
import com.arcsoft.face.toolkit.ImageInfo;
import com.li.common.manager.FaceEngineFactory;
import com.li.model.mapper.FaceUserDao;
import com.li.model.service.FaceEngineService;
import org.apache.commons.pool2.impl.GenericObjectPool;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * <h3>thermal_imaging</h3>
 * <p></p>
 *
 * @author : 李星鹏
 * @date : 2020-12-10 15:08
 **/
@Service
public class FaceEngineServiceImpl implements FaceEngineService {
    @Value("${config.arcface-sdk.sdk-lib-path}")
    public String sdkLibPath;
    @Value("${config.arcface-sdk.app-id}")
    public String appId;

    @Value("${config.arcface-sdk.sdk-key}")
    public String sdkKey;

    @Value("${config.arcface-sdk.thread-pool-size}")
    public Integer threadPoolSize;

    @Autowired
    private FaceUserDao faceUserDao;

    /**
     *异步执行的机制 后台执行
     */
    private ExecutorService executorService;

    /**
     * FaceEngine对象池
     */
    private GenericObjectPool<FaceEngine> faceEngineObjectPool;

    /**
     * 启动时执行 当且仅当执行一次
     */
    @PostConstruct
    public void init(){
        //创建线程个数
        executorService= Executors.newFixedThreadPool(threadPoolSize);
        //创建对象池配置
        GenericObjectPoolConfig genericObjectPool = new GenericObjectPoolConfig();
        //设置池闲置的最大容量
        genericObjectPool.setMaxIdle(threadPoolSize);
        //设置池的大小
        genericObjectPool.setMaxTotal(threadPoolSize);
        //设置池闲置的最小容量
        genericObjectPool.setMinIdle(threadPoolSize);
        //关闭对象池默认配置
        genericObjectPool.setLifo(false);

        /**
         * 人脸识别引擎
         */
        EngineConfiguration engineConfiguration = new EngineConfiguration();
        engineConfiguration.setDetectMode(DetectMode.ASF_DETECT_MODE_IMAGE);
        engineConfiguration.setDetectFaceOrientPriority(DetectOrient.ASF_OP_0_ONLY);

        /**
         * 功能配置
         */
        FunctionConfiguration functionConfiguration = new FunctionConfiguration();
        functionConfiguration.setSupportFace3dAngle(true);
        functionConfiguration.setSupportFaceDetect(true);
        functionConfiguration.setSupportFaceRecognition(true);
        functionConfiguration.setSupportLiveness(true);
        functionConfiguration.setSupportIRLiveness(true);
        engineConfiguration.setFunctionConfiguration(functionConfiguration);

        //创建引擎对象池
        faceEngineObjectPool= new GenericObjectPool(new FaceEngineFactory(sdkLibPath, appId, sdkKey, engineConfiguration), genericObjectPool);
    }

    private int plusHundred(Float value) {
        BigDecimal target = new BigDecimal(value);
        BigDecimal hundred = new BigDecimal(100f);
        return target.multiply(hundred).intValue();
    }

    /**
     * 人脸特征提取
     * @param imageInfo
     * @return
     * @throws InterruptedException
     */
    @Override
    public byte[] extractFaceFeature(ImageInfo imageInfo) throws InterruptedException {
        FaceEngine faceEngine=null;
        try{
            faceEngine=faceEngineObjectPool.borrowObject();
            List<FaceInfo> faceInfoList=new LinkedList<>();
            //人脸检测
            int i=faceEngine.detectFaces(imageInfo.getImageData(),imageInfo.getWidth(),imageInfo.getHeight(),imageInfo.getImageFormat(),faceInfoList);

            //提取人脸数据
            if(CollectionUtil.isNotEmpty(faceInfoList)){
                FaceFeature faceFeature=new FaceFeature();
                faceEngine.extractFaceFeature(imageInfo.getImageData(),imageInfo.getWidth(),imageInfo.getHeight(),imageInfo.getImageFormat(),faceInfoList.get(0),faceFeature);
                return faceFeature.getFeatureData();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            if (faceEngine != null) {
                //释放引擎对象
                faceEngineObjectPool.returnObject(faceEngine);
            }
        }
        return null;
    }

}

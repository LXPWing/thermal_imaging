package com.li.model.mapper;

import com.li.model.vo.UserInfoVO;
import com.li.model.vo.ExcelVO;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
@Mapper
public interface AdminDao {
    /**
     * 获取用户体温
     * @param name 用户名
     * @return
     */
    Map getUserTemperature(String name);

    /**
     * 获取全部用户名(分页)
     * @return
     */
    List<UserInfoVO> getAllUserInfo(Integer start, Integer size);

    /**
     * 数据的总数
     */
    Long getTotalCounts();

    /**
     * 获取Execl信息
     * @return
     */
    List<ExcelVO> getExecl();


}

package com.li.model.vo;

import lombok.Data;

/**
 * <h3>thermal_imaging</h3>
 * <p></p>
 *
 * @author : 李星鹏
 * @date : 2021-01-18 22:24
 **/
@Data
public class UserInfoVO {
    private String name;
    private String sex;
    //private Integer age;
    //private String country;
    private String address;
    private String status;
}

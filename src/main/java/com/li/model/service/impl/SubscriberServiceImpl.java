package com.li.model.service.impl;

import com.li.common.exception.ApiAssert;
import com.li.common.exception.ApiException;
import com.li.model.dto.UserOpinion;
import com.li.model.dto.UserTemp;
import com.li.model.mapper.SubDao;
import com.li.model.service.SubscriberService;
import com.li.util.AjaxResult;
import com.li.util.DateUtil;
import com.li.util.MongoUtil;
import com.li.util.UUIDUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.Date;

/**
 * <h3>thermal_imaging</h3>
 * <p></p>
 *
 * @author : 李星鹏
 * @date : 2021-01-10 22:00
 **/
@Service
@Slf4j
public class SubscriberServiceImpl implements SubscriberService {

    @Autowired
    private MongoUtil mongoUtil;

    @Autowired
    private SubDao subDao;

    @Override
    public String getTalk(String str) {
        URL url=null;
        InputStream inputStream=null;
        URLConnection urlConnection=null;
        InputStreamReader inputStreamReader=null;
        BufferedReader bufferedReader=null;
        String read=null;
        try {
            //String encode = URLEncoder.encode("", "UTF-8");
            url = new URL("http://api.qingyunke.com/api.php?key=free&appid=0&msg="+str);
            urlConnection = url.openConnection();
            //urlConnection.setConnectTimeout(1000);
            inputStream = urlConnection.getInputStream();
            inputStreamReader = new InputStreamReader(inputStream);
            bufferedReader = new BufferedReader(inputStreamReader);
            read = bufferedReader.readLine();
            ApiAssert.notNull(read,"服务器异常");
        } catch (Exception e) {
            //System.out.println("捕获成功");
            e.printStackTrace();
            throw new ApiException("服务器异常");
        }finally {
            try {
                bufferedReader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        //log.info(read);
        int i = read.lastIndexOf("\"");
        String substring = read.substring(23, i);
        return substring;
    }

    @Override
    public void sendOpinion(String msg,String username) {
        UserOpinion userOpinion = new UserOpinion();
        userOpinion.setId(UUIDUtil.randUUID());
        userOpinion.setUsername(username);
        userOpinion.setUserOpinion(msg);
        userOpinion.setData(DateUtil.format(new Date(),"yyyy-MM-dd HH:mm:ss"));
        userOpinion.setStatus("未解决");
        log.info(String.valueOf(userOpinion));
        mongoUtil.setUserOpiDoc(userOpinion);
    }

    @Override
    public UserTemp getUserTemp(String username) {
        UserTemp userTemp = subDao.getUserTemp(username);
        return userTemp;
    }

}

package com.li.model.service;

import com.li.model.dto.UserTemp;

public interface SubscriberService {
    /**
     * 机器人交谈
     * @param str
     * @return
     */
    String getTalk(String str);

    /**
     * 意见反馈
     * @param msg 消息
     * @param username 用户名
     */
    void sendOpinion(String msg,String username);

    UserTemp getUserTemp(String username);
}

package com.li.model.vo;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * <h3>thermal_imaging</h3>
 * <p>获取人脸信息</p>
 *
 * @author : 李星鹏
 * @date : 2020-12-09 22:37
 **/
@Data
@NoArgsConstructor
public class UserFaceInfoVO implements Serializable {
    private String username;
    private String faceId;
}

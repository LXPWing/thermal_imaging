package com.li.model.service;

import com.li.model.dto.User;

public interface RegisterService {
    void register(User user);
}

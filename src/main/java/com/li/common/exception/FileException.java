package com.li.common.exception;

/**
 * <h3>thermal_imaging</h3>
 * <p></p>
 *
 * @author : 李星鹏
 * @date : 2021-05-07 15:41
 **/
public class FileException extends BaseException{
    public FileException(String code,Object[] args){
        super("file",code,args,null);
    }
}

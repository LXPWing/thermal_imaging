package com.li.model.mapper;

import com.li.model.dto.UserTemp;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface SubDao {

    UserTemp getUserTemp(String username);

}

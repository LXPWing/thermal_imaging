package com.li.config;

import com.li.interceptor.PhysicalPageInterceptor;
import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

import javax.sql.DataSource;

/**
 * <h3>thermal_imaging</h3>
 * <p>为mybatis设置sqlSession及mapper.xml配置</p>
 *
 * @author : 李星鹏
 * @date : 2020-12-04 14:48
 **/
@Configuration
public class MybatisConfiguration {
    @Bean
    public SqlSessionFactory sqlSessionFactory(DataSource dataSource) throws Exception {
        SqlSessionFactoryBean bean = new SqlSessionFactoryBean();
        bean.setDataSource(dataSource);
        bean.setConfiguration(new org.apache.ibatis.session.Configuration());
        // 加入sql语句拦截器
        bean.setPlugins(new Interceptor[] {new PhysicalPageInterceptor()});
        bean.setMapperLocations(new PathMatchingResourcePatternResolver().getResources("classpath:Mapper/*.xml"));
        return bean.getObject();
    }
}

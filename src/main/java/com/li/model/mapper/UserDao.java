package com.li.model.mapper;

import com.li.model.dto.User;
import com.li.model.dto.UserFaceInfo;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface UserDao  {

    int saveUser(Map map);

    List<User> getUserName(Map map);

    int saveFaceId(UserFaceInfo userFaceInfo);
}

package com.li.model.controller;

import com.li.model.service.RegisterService;
import com.li.model.dto.User;
import com.li.util.AjaxResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

/**
 * <h3>thermal_imaging</h3>
 * <p>注册</p>
 *
 * @author : 李星鹏
 * @date : 2020-12-15 20:08
 **/
@RestController
@RequestMapping("/sys")
@Api("APP注册")
@Slf4j
public class RegisterController {
    @Autowired
    private RegisterService register;

    @PostMapping("/register")
    @ApiOperation("注册")
    public AjaxResult toRegister(@Valid User user){
        log.info("调用Register");
        register.register(user);
        return AjaxResult.ok();
    }

}

/*
SQLyog Ultimate v12.09 (64 bit)
MySQL - 5.7.19 : Database - thermal_imaging
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`thermal_imaging` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_bin */;

USE `thermal_imaging`;

/*Table structure for table `sys_admin` */

DROP TABLE IF EXISTS `sys_admin`;

CREATE TABLE `sys_admin` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `userename` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '用户名',
  `password` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `role` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `role` (`role`),
  CONSTRAINT `sys_admin_ibfk_1` FOREIGN KEY (`role`) REFERENCES `sys_role` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

/*Data for the table `sys_admin` */

/*Table structure for table `sys_perms` */

DROP TABLE IF EXISTS `sys_perms`;

CREATE TABLE `sys_perms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `value` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `permission_code` bigint(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

/*Data for the table `sys_perms` */

insert  into `sys_perms`(`id`,`name`,`value`,`permission_code`) values (1,'查看反馈','admin_user:select',10),(2,'删除反馈','admin_user:delete',10),(3,'后台用户列表','admin_user:list',10),(4,'更新反馈','admin——user:update',10);

/*Table structure for table `sys_role` */

DROP TABLE IF EXISTS `sys_role`;

CREATE TABLE `sys_role` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

/*Data for the table `sys_role` */

insert  into `sys_role`(`id`,`name`) values (1,'common'),(2,'custos');

/*Table structure for table `sys_role_perms` */

DROP TABLE IF EXISTS `sys_role_perms`;

CREATE TABLE `sys_role_perms` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `roleid` int(6) DEFAULT NULL,
  `permsid` int(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `permsid` (`permsid`),
  KEY `sys_role_perms_ibfk_1` (`roleid`),
  CONSTRAINT `sys_role_perms_ibfk_1` FOREIGN KEY (`roleid`) REFERENCES `sys_role` (`id`),
  CONSTRAINT `sys_role_perms_ibfk_2` FOREIGN KEY (`permsid`) REFERENCES `sys_perms` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

/*Data for the table `sys_role_perms` */

insert  into `sys_role_perms`(`id`,`roleid`,`permsid`) values (1,2,1),(2,2,2),(3,2,3);

/*Table structure for table `sys_user_face` */

DROP TABLE IF EXISTS `sys_user_face`;

CREATE TABLE `sys_user_face` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL COMMENT '用户名',
  `face_id` text COLLATE utf8mb4_bin COMMENT '人脸唯一Id',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `updata_time` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`,`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

/*Data for the table `sys_user_face` */

/*Table structure for table `sys_user_information` */

DROP TABLE IF EXISTS `sys_user_information`;

CREATE TABLE `sys_user_information` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '编号',
  `username` varchar(255) COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '用户名',
  `password` varchar(255) COLLATE utf8mb4_bin DEFAULT '' COMMENT '密码',
  `name` varchar(255) COLLATE utf8mb4_bin DEFAULT '' COMMENT '姓名',
  `sex` int(2) DEFAULT '0' COMMENT '性别',
  `number` varchar(255) COLLATE utf8mb4_bin DEFAULT '' COMMENT '身份证号',
  `address` varchar(255) COLLATE utf8mb4_bin DEFAULT '' COMMENT '地址',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `status` varchar(2) COLLATE utf8mb4_bin DEFAULT '正常' COMMENT '体温状态',
  `role` int(20) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`,`username`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

/*Data for the table `sys_user_information` */

insert  into `sys_user_information`(`id`,`username`,`password`,`name`,`sex`,`number`,`address`,`create_time`,`update_time`,`status`,`role`) values (1,'lxp','123','lxp',1,'1371568','随便','2020-12-18 16:47:09','2020-12-18 16:47:09','正常',1),(2,'lxx','456','小明',2,'342545645','随便','2020-12-18 16:49:11','2020-12-18 16:49:11','正常',1),(4,'qweqwewq','qwe','wqewq',1,'qweqw','ewqeqw','2020-12-27 11:23:10','2020-12-27 11:23:10','正常',1),(5,'kjhkhjk','lkjl','kjlkj',1,'lkjlj','kjljk','2020-12-27 11:35:02','2020-12-27 11:35:02','正常',1),(6,'kjhkhjk','lkjl','kjlkj',1,'lkjlj','kjljk','2020-12-27 11:35:39','2020-12-27 11:35:39','正常',1),(7,'kjhkhjk','lkjl','kjlkj',1,'lkjlj','kjljk','2020-12-27 11:35:46','2020-12-27 11:35:46','正常',1),(9,'kjhkhjk','lkjl','kjlkj',1,'lkjlj','kjljk','2020-12-27 11:44:28','2020-12-27 11:44:28','正常',1),(10,'asdasdas','asdsa','dasda',1,'dasasd','sadas','2020-12-27 11:45:55','2020-12-27 11:45:55','正常',1),(11,'asdasdas','asdsa','dasda',1,'dasasd','sadas','2020-12-27 11:51:05','2020-12-27 11:51:05','正常',1),(12,'asdasdas','asdsa','dasda',1,'dasasd','sadas','2020-12-27 11:52:18','2020-12-27 11:52:18','正常',1),(13,'21321','qwewqe','qwe',1,'qweqwe','qwewq','2021-03-20 11:11:00','2021-03-20 11:11:00','正常',1);

/*Table structure for table `sys_user_temperature` */

DROP TABLE IF EXISTS `sys_user_temperature`;

CREATE TABLE `sys_user_temperature` (
  `name` varchar(225) COLLATE utf8mb4_bin NOT NULL COMMENT '用户名',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `temperature` decimal(3,1) NOT NULL COMMENT '温度'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

/*Data for the table `sys_user_temperature` */

insert  into `sys_user_temperature`(`name`,`create_time`,`temperature`) values ('lxp','2021-01-08 17:03:27','36.7'),('lxp','2021-01-08 17:03:35','38.0');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

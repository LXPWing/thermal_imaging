package com.li.model.dto;

import lombok.Data;

/**
 * <h3>thermal_imaging</h3>
 * <p></p>
 *
 * @author : 李星鹏
 * @date : 2021-01-24 19:16
 **/
@Data
public class UserFaceInfo {
    private Integer id;
    private String name;
    private String face_id;
}

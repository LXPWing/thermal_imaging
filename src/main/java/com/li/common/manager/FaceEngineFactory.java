package com.li.common.manager;

import com.arcsoft.face.EngineConfiguration;
import com.arcsoft.face.FaceEngine;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.pool2.BasePooledObjectFactory;
import org.apache.commons.pool2.PooledObject;
import org.apache.commons.pool2.impl.DefaultPooledObject;

/**
 * <h3>thermal_imaging</h3>
 * <p>人脸识别引擎工厂</p>
 *
 * @author : 李星鹏
 * @date : 2020-12-09 22:01
 **/
@Slf4j
public class FaceEngineFactory extends BasePooledObjectFactory<FaceEngine> {
    private String appId;
    private String sdkKey;
    private String sdkLibPath;
    private EngineConfiguration engineConfiguration;

    public FaceEngineFactory(String appId, String sdkKey, String sdkLibPath, EngineConfiguration engineConfiguration) {
        this.appId = appId;
        this.sdkKey = sdkKey;
        this.sdkLibPath = sdkLibPath;
        this.engineConfiguration = engineConfiguration;
    }

    /**
     * 创建对象
     * @return
     * @throws Exception
     */
    @Override
    public FaceEngine create() throws Exception {
        FaceEngine faceEngine = new FaceEngine(sdkLibPath);
        int i = faceEngine.activeOnline(appId, sdkKey);
        log.info("SDK在线激活:"+i);
        int init = faceEngine.init(engineConfiguration);
        log.info("获取初始值:"+init );
        return faceEngine;
    }

    /**
     * 对对象进行池化操作
     * @param obj
     * @return
     */
    @Override
    public PooledObject<FaceEngine> wrap(FaceEngine obj) {
        return new DefaultPooledObject<>(obj);
    }

    @Override
    public void destroyObject(PooledObject<FaceEngine> p) throws Exception {
        FaceEngine object = p.getObject();
        int i = object.unInit();
        log.info("销毁引擎:" + i);
        super.destroyObject(p);
    }

}

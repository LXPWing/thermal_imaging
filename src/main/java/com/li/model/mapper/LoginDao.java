package com.li.model.mapper;

import com.li.model.dto.SimpleUser;
import com.li.model.dto.User;
import com.li.model.vo.LoginUserVO;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface LoginDao {
    SimpleUser getLoginMG(String username);

    Integer getUserId(String username);
}

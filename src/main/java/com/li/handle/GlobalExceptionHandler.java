package com.li.handle;

import com.li.common.exception.ApiException;
import com.li.common.exception.BaseException;
import com.li.util.AjaxResult;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.ArrayList;
import java.util.List;

/**
 * <h3>thermal_imaging</h3>
 * <p></p>
 *
 * @author : 李星鹏
 * @date : 2021-05-07 15:52
 **/
@RestControllerAdvice
//@Slf4j
public class GlobalExceptionHandler {
    private static final Logger log= LoggerFactory.getLogger(GlobalExceptionHandler.class);

    /**
     * 基础异常
     * @param e
     * @return
     */
    @ExceptionHandler(BaseException.class)
    public AjaxResult baseException(BaseException e){
        return AjaxResult.error(e.getMessage());
    }

    /**
     * 接口同一错误处理
     * @param e
     * @return
     */
    @ExceptionHandler(ApiException.class)
    public AjaxResult jsonErrorHandle(ApiException e){
        log.error(e.getMsg());
        return AjaxResult.error(e.getCode(),e.getMsg());
    }

    /**
     * 数据校验异常
     * @param exception
     * @return
     */
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public AjaxResult vailHandler(MethodArgumentNotValidException exception){
        BindingResult bindingResult = exception.getBindingResult();
        final List<FieldError> fieldErrorList=bindingResult.getFieldErrors();
        List<String> error=new ArrayList<>();
        for(FieldError e:fieldErrorList){
            error.add(e.getDefaultMessage());
        }
        return AjaxResult.error(error);
    }
}

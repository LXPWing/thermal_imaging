package com.li.model.service.impl;

import com.li.common.exception.ApiAssert;
import com.li.config.RedisCache;
import com.li.model.vo.LoginUserVO;
import com.li.model.mapper.LoginDao;
import com.li.model.service.LoginService;
import com.li.util.Constants;
import com.li.util.AjaxResult;
import com.li.util.ShiroUtil;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * <h3>thermal_imaging</h3>
 * <p>]</p>
 *
 * @author : 李星鹏
 * @date : 2020-12-16 19:01
 **/
@Service
public class LoginServiceImpl implements LoginService {

    @Autowired
    private LoginDao loginDao;

    @Autowired
    private RedisCache redisCache;

    @Override
    public AjaxResult Login(LoginUserVO loginUserVO) {
        String userName= loginUserVO.getUsername();
        String userPwd= loginUserVO.getPassword();
        String uuid= loginUserVO.getUuid();
        String code = loginUserVO.getCode();

//        ApiAssert.isEmpty(uuid,"uuid不能为空");
//        ApiAssert.isEmpty(code,"code不能为空");

        String verifyKey = Constants.CAPTCHA_CODE_KEY + uuid;
        String captcha = redisCache.getCacheObject(verifyKey);

        Integer userId = loginDao.getUserId(userName);

        if(code.equals(captcha)){
            Subject subject = ShiroUtil.getSubject();
            if(!subject.isAuthenticated()) {
                UsernamePasswordToken token = new UsernamePasswordToken(userName, userPwd);
//                try{
                subject.login(token);
                AjaxResult ajaxResult = new AjaxResult();
                ajaxResult.put("userId", userId);
                return ajaxResult;
            }
//                }catch (Exception e){
//                    return AjaxResult.error("用户或密码错误");
//                }
//            }else {
//                return AjaxResult.error("请登录，再执行操作");
//            }
        }
        return AjaxResult.error("验证码错误");
    }


}

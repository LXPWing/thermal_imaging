package com.li.common.exception;

import cn.hutool.core.util.StrUtil;
import org.springframework.util.Assert;

/**
 * <h3>thermal_imaging</h3>
 * <p></p>
 *
 * @author : 李星鹏
 * @date : 2021-05-08 18:28
 **/
public class ApiAssert extends Assert {

    public static void isNull(Object o,String msg){
        if(o!=null){
            throw new ApiException(msg);
        }
    }

    public static void isNull(Object o,String msg, int code){
        if(o!=null){
            throw new ApiException(msg,code);
        }
    }

    public static void notNull(Object o,String msg){
        if(o==null){
            throw new ApiException(msg);
        }
    }

    public static void notNull(Object o,String msg,int code){
        if(o==null){
            throw new ApiException(msg,code);
        }
    }

    public static void isTrue(boolean e,String msg){
        if(!e){
            throw new ApiException(msg);
        }
    }

    public static void isTrue(boolean e,int code ,String msg){
        if(!e){
            throw new ApiException(msg,code);
        }
    }

    public static void notTrue(boolean e,String msg){
        if(e){
            throw new ApiException(msg);
        }
    }

    public static void notTrue(boolean e,String msg,int code){
        if(e){
            throw new ApiException(msg,code);
        }
    }

    public static void isEmpty(String s,String msg){
        if(!StrUtil.isEmpty(s)){
            throw new ApiException(msg);
        }
    }

    public static void isEmpty(String s, String msg,int code){
        if(StrUtil.isEmpty(s)){
            throw new ApiException(msg,code);
        }
    }

    public static void notEmpty(String s,String msg){
        if(StrUtil.isEmpty(s)){
            throw new ApiException(msg);
        }
    }

    public static void notEmpty(String s, String msg,int code){
        if(StrUtil.isEmpty(s)){
            throw new ApiException(msg,code);
        }
    }
}

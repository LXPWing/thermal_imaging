package com.li;

import com.li.model.dto.Permission;
import com.li.model.dto.RolePermission;
import com.li.model.mapper.PermissionDao;
import com.li.model.mapper.RolePermissionDao;
import com.li.model.vo.ExcelVO;
import com.li.model.mapper.AdminDao;
import com.li.model.mapper.UserDao;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.web.bind.annotation.RequestMethod;

import java.io.IOException;
import java.util.List;

@SpringBootTest
class ThermalImagingApplicationTests {

    static String filePath="C:\\ABC\\";

    @Autowired
    private UserDao userDao;

    @Autowired
    private AdminDao adminDao;

    @Autowired
    private RolePermissionDao rolePermissionDao;

    @Autowired
    private PermissionDao permissionDao;

    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private ShiroFilterFactoryBean shiroFilterFactoryBean;

    @Test
    void contextLoads() throws IOException {
        List<ExcelVO> execl = adminDao.getExecl();
//        List<getExcel> p=new ArrayList<>();
////        List allUserInfo = adminDao.getAllUserInfo(1, 1);
////        System.out.println(allUserInfo);
//        for (ExcelData o : execl) {
//            getExcel getExcel = new getExcel();
//            getExcel.setName(o.getName());
//            getExcel.setSex(o.getSex());
//            getExcel.setAddress(o.getAddress());
//            getExcel.setNumber(o.getNumber());
//            List<TimeAndTemp> userStatus = o.getUserStatus();
//
//            for (TimeAndTemp status : userStatus) {
////                getExcel.setTemperature(status.getTemperature());
////                getExcel.setTime(status.);
//            }
//
//        }

//        String path=filePath+"simpleWrite.xlsx";
//
//        EasyExcel.write(path,ExcelData.class).sheet("info").doWrite(execl);
        for (ExcelVO excelData : execl) {
            System.out.println(excelData);
        }
    }


    @Test
    void mongodb(){
//        com.li.Test test = new com.li.Test();
//        test.setAge(12);
//        test.setId(12);
//        test.setName("lxp");

        System.out.println(shiroFilterFactoryBean==null);
    }

    @Test
    void suibian(){
        String name = RequestMethod.OPTIONS.name();
        System.out.println(name);

    }

    @Test
    void Role(){
//        List<RolePermission> rolePermissionById = rolePermissionDao.getRolePermissionById(2);
//        for(RolePermission rolePermission: rolePermissionById){
//            System.out.println(rolePermission);
//        }
        List<Permission> permissionList = permissionDao.getPermissionList(2);
        for(Permission rolePermission: permissionList){
            System.out.println(rolePermission);
        }

    }
}

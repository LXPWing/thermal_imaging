package com.li.model.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * <h3>thermal_imaging</h3>
 * <p> 时间和温度</p>
 *
 * @author : 李星鹏
 * @date : 2021-02-03 19:51
 **/
@Data
public class TimeAndTemp {
    private LocalDateTime create_time;
    private BigDecimal temperature;
    //private String status;
}

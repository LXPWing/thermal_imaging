package com.li.model.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * <h3>thermal_imaging</h3>
 * <p></p>
 *
 * @author : 李星鹏
 * @date : 2021-01-24 20:33
 **/
@Data
public class TempAndTime {
    private Date date;
    private BigDecimal temp;
}

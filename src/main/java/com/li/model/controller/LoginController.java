package com.li.model.controller;

import com.li.model.vo.LoginUserVO;
import com.li.model.service.LoginService;
import com.li.util.AjaxResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * <h3>thermal_imaging</h3>
 * <p>登入</p>
 *
 * @author : 李星鹏
 * @date : 2020-11-28 20:45
 **/

@RestController
@RequestMapping("/sys")
@Api("APP登入接口")
public class LoginController {


    @Autowired
    private LoginService loginService;

    @PostMapping("/login")
    @ApiOperation("登入")
    public AjaxResult login(@Valid @RequestBody LoginUserVO loginuser){
        return loginService.Login(loginuser);
    }


}

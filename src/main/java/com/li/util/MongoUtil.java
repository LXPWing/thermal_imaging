package com.li.util;

import com.li.model.dto.UserOpinion;
import com.mongodb.client.result.DeleteResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * <h3>thermal_imaging</h3>
 * <p></p>
 *
 * @author : 李星鹏
 * @date : 2021-01-23 22:27
 **/
@Component
public class MongoUtil {
    @Autowired
    private MongoTemplate mongoTemplate;

    @Value("${thermal.mongoUserOpi}")
    private String userOpiDoc;

    @Value("${thermal.mongoLogger}")
    private String logDoc;

    /**
     * 插入用户反馈
     * @param value 反馈内容
     * @param <T>
     */
    public <T> void setUserOpiDoc(final T value){
        mongoTemplate.insert(value,userOpiDoc);
    }

    /**
     * 插入应用日志
     * @param value
     * @param <T>
     */
    public <T> void setLogger(final T value){
        mongoTemplate.insert(value,logDoc);
    }

    /**
     * 获取全部用户反馈
     * @param t
     * @param <T>
     * @return
     */
    public <T> List<T> findAllUserOpi(final T t){
        return (List<T>) mongoTemplate.findAll(t.getClass(),userOpiDoc);
    }

    /**
     * 更新用户反馈状态
     * @param id
     */
    public void updateUserOpi(final String id){
        Criteria criteria=Criteria.where("id").is(id);
        Query query = new Query(criteria);
        Update update = new Update()
                .set("status","已解决");
        mongoTemplate.updateFirst(query, update,UserOpinion.class,userOpiDoc);
    }

    /**
     * 删除已解决反馈
     */
    public void deleteStatus(){
        Criteria criteria=Criteria.where("status").is("已解决");
        Query query=new Query(criteria);
        DeleteResult remove = mongoTemplate.remove(query, userOpiDoc);
    }


}

package com.li.model.service;


import com.arcsoft.face.toolkit.ImageInfo;

public interface FaceEngineService {
    //人脸
   // List<FaceUserInfo> compareFaceFeature(byte[] faceFeature, Integer groupId) throws InterruptedException, ExecutionException;

    /**
     * 人脸特征
     * @param imageInfo
     * @return
     */
    byte[] extractFaceFeature(ImageInfo imageInfo) throws InterruptedException;
}

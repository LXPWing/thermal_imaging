package com.li.model.dto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.*;
import java.io.Serializable;


/**
 * <h3>thermal_imaging</h3>
 * <p>用户注册实体类</p>
 *
 * @author : 李星鹏
 * @date : 2020-11-23 20:03
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
public class User implements Serializable {
//    private Integer id;
    @NotEmpty(message = "用户名不能为空")
    @Size(min = 1,max = 7,message = "用户名不符合")
    private String username;

    @NotEmpty(message = "密码不能为空")
    @Size(min = 6,max = 15,message = "密码不符合")
    private String password;

    @NotEmpty(message = "姓名不能为空")
    @Size(min = 1,max = 7,message = "姓名不符合")
    private String name;

//    @NotEmpty(message = "年龄不能为空")
//    @DecimalMin(value = "1",message = "年龄错误")
//    @DecimalMax(value = "200",message = "年龄错误")
//    private Integer age;

    @NotNull(message = "性别不能为空")
    private Integer sex;

//    @Email(message = "邮箱格式不符合")
//    @NotEmpty(message = "邮箱不能为空")
//    private String email;

    @NotEmpty(message = "电话号码不能为空")
    @Size(min = 1,max = 20,message = "电话号码不符合")
    private String number;

//    @NotEmpty(message = "国籍不能为空")
//    private String country;

    @NotEmpty(message = "地址不能为空")
    private String address;

    @NotEmpty(message = "照片不能为空")
    private String face;

//    private Date createTime;
//    private Date updateTime;

}

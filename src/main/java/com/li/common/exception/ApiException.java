package com.li.common.exception;

/**
 * <h3>thermal_imaging</h3>
 * <p>自定义异常</p>
 *
 * @author : 李星鹏
 * @date : 2020-12-16 21:47
 **/
public class ApiException extends RuntimeException{
    private String msg;
    private int code=201;


    public ApiException(String msg) {
        super(msg);
        this.msg = msg;
    }

    public ApiException(String msg, Throwable e) {
        super(msg, e);
        this.msg = msg;
    }

    public ApiException(String msg, int code, Throwable e) {
        super(msg, e);
        this.msg = msg;
        this.code = code;
    }

    public ApiException(String msg, int code) {
        this.msg = msg;
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}

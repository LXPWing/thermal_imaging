package com.li.model.mapper;

import com.li.model.dto.Permission;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface PermissionDao {
    List<Permission> getPermissionList(Integer roleId);
}

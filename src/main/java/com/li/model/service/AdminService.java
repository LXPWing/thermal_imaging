package com.li.model.service;

import com.li.model.vo.UserInfoVO;
import com.li.model.dto.UserOpinion;

import java.util.List;

public interface AdminService {
    /**
     * 获取用户信息
     * @param page 当前页
     * @param rows 查询行数
     * @return
     */
    List<UserInfoVO> getAllName(Integer page, Integer rows);

    /**
     * 获取总用户数
     * @return
     */
    Long getUserCounts();

    /**
     * 获取全部反馈
     * @return
     */
    List<UserOpinion>  getUserOpinion();

    /**
     * 更新反馈状态
     */
    void updateOpinionStatus(String id);

    /**
     * 删除已解决反馈
     */
    void deleteSolved();



}

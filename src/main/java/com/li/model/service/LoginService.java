package com.li.model.service;

import com.li.model.vo.LoginUserVO;
import com.li.util.AjaxResult;

public interface LoginService {
    AjaxResult Login(LoginUserVO loginUserVO);
}

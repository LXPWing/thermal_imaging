package com.li.config;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * <h3>thermal_imaging</h3>
 * <p></p>
 *
 * @author : 李星鹏
 * @date : 2020-11-28 22:07
 **/
public class ShiroFilterMapFactory {

    public static final Map<String,String> shiroFilterMap(){
        Map<String,String> map=new LinkedHashMap<>();
        map.put("/sys/login","anon");
        map.put("/sys/register","anon");
        map.put("/swagger-ui.html", "anon");
        map.put("/swagger-resources/**", "anon");
        map.put("/v2/api-docs", "anon");
        map.put("/webjars/springfox-swagger-ui/**", "anon");
        map.put("/sys/**","jwt");
        return map;
    }

}

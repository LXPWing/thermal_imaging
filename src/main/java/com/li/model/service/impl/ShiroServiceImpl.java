package com.li.model.service.impl;

import com.li.model.dto.Permission;
import com.li.model.dto.RolePermission;
import com.li.model.dto.SimpleUser;
import com.li.model.dto.User;
import com.li.model.mapper.LoginDao;
import com.li.model.mapper.PermissionDao;
import com.li.model.service.ShiroService;
import com.li.model.vo.LoginUserVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <h3>thermal_imaging</h3>
 * <p></p>
 *
 * @author : 李星鹏
 * @date : 2020-12-16 22:02
 **/
@Service
public class ShiroServiceImpl implements ShiroService {
    private static Map<String,List<Permission>> permissionByRoleId=new HashMap<>();

    @Autowired
    private LoginDao loginDao;

    @Autowired
    private PermissionDao permissionDao;

    @Override
    public SimpleUser getLoginMG(String username) {
        SimpleUser loginMG = loginDao.getLoginMG(username);
        return loginMG;
    }

    @Override
    public List<Permission> selectByRoledId(Integer roleid) {
        if(permissionByRoleId.get("roled_"+roleid)!=null){
            return permissionByRoleId.get("roled_"+roleid);
        }
        List<Permission> permissionList = permissionDao.getPermissionList(roleid);
        permissionByRoleId.put("role_"+roleid,permissionList);
        return permissionList;
    }


}

package com.li;

import io.swagger.models.auth.In;
import lombok.Data;

/**
 * <h3>thermal_imaging</h3>
 * <p></p>
 *
 * @author : 李星鹏
 * @date : 2021-01-23 21:56
 **/
@Data
public class Test {
    private Integer id;
    private String name;
    private Integer age;
}

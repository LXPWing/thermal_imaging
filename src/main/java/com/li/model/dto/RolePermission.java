package com.li.model.dto;


import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * <h3>thermal_imaging</h3>
 * <p></p>
 *
 * @author : 李星鹏
 * @date : 2021-05-07 17:28
 **/
@Data
@AllArgsConstructor
public class RolePermission{
    private Integer roleId;
    private Integer permissionId;
}

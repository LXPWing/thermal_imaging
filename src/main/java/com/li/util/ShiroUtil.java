package com.li.util;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;

/**
 * <h3>thermal_imaging</h3>
 * <p></p>
 *
 * @author : 李星鹏
 * @date : 2020-12-17 22:55
 **/
public class ShiroUtil {
   public static Session getSession(){
       return SecurityUtils.getSubject().getSession();
   }

   public static Subject getSubject(){
       return SecurityUtils.getSubject();
   }

   public static Boolean isLogin(){
       return SecurityUtils.getSubject().getPrincipal()!=null;
   }

   public static void setSessionAttribute(Object key,Object value){
       getSession().setAttribute(key, value);
   }

   public static Object getSessionAttribute(Object key){
       return getSession().getAttribute(key);
   }


}

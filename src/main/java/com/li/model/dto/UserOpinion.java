package com.li.model.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * <h3>thermal_imaging</h3>
 * <p></p>
 *
 * @author : 李星鹏
 * @date : 2021-01-23 22:02
 **/
@Data
public class UserOpinion implements Serializable {
    private String id;
    private String username;
    private String data;
    private String userOpinion;
    private String status;
}

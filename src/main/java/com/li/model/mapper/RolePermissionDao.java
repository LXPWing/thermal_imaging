package com.li.model.mapper;

import com.li.model.dto.RolePermission;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
@Mapper
@Repository
public interface RolePermissionDao {
    List<RolePermission> getRolePermissionById(Integer roleId);
}

package com.li.model.controller;

import com.li.model.service.AdminService;
import com.li.model.vo.UserInfoVO;
import com.li.model.dto.UserOpinion;
import com.li.util.AjaxResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <h3>thermal_imaging</h3>
 * <p></p>
 *
 * @author : 李星鹏
 * @date : 2021-01-04 13:07
 **/
@RestController
@RequestMapping("/sys")
@Api("管理员")
@Slf4j
public class AdminController {
    private static final Logger logger= LoggerFactory.getLogger(AdminController.class);

    @Autowired
    private AdminService adminService;

    @GetMapping("/admin/getAllUser")
    @ApiOperation("获取全部用户")
    @RequiresPermissions("admin_user:list")
    public AjaxResult getAllUserName(Integer page, Integer rows){
        //查询总条数
        Long userCounts = adminService.getUserCounts();

        List<UserInfoVO> allName = adminService.getAllName(page, rows);

        //总页数
        Long totalPage=userCounts%rows==0?userCounts/rows:userCounts/rows+1;

        AjaxResult ajaxResult = new AjaxResult();
        ajaxResult.put("page",page);
        ajaxResult.put("rows",allName);
        ajaxResult.put("total",userCounts);
        ajaxResult.put("records",totalPage);
        return ajaxResult;
    }

    @GetMapping("/admin/getUserInfo")
    @ApiOperation("获取用户信息")
    public AjaxResult getUserInfo(String name){

        return AjaxResult.ok();
    }

    @GetMapping("/admin/getOpinion")
    @ApiOperation("获取用户反馈")
    @RequiresPermissions("admin_user:select")
    public AjaxResult getOpinion(){
        List<UserOpinion> userOpinion = adminService.getUserOpinion();
        AjaxResult ajaxResult = new AjaxResult();
        ajaxResult.put("msg",userOpinion);
        return ajaxResult;
    }

    @PostMapping("/admin/updateStatus")
    @ApiOperation("更新反馈状态")
    @RequiresPermissions("admin_user:update")
    public AjaxResult updateStatus(String id){
        log.info(id);
        adminService.updateOpinionStatus(id);
        return AjaxResult.ok();
    }

    @DeleteMapping("/admin/deleteSolved")
    @ApiOperation("删除已解决反馈")
    @RequiresPermissions("admin_user:delete")
    public AjaxResult deleteOpi(){
        adminService.deleteSolved();
        return AjaxResult.ok();
    }


}

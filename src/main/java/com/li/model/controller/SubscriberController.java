package com.li.model.controller;

import com.li.model.dto.TempAndTime;
import com.li.model.dto.UserTemp;
import com.li.model.service.SubscriberService;
import com.li.util.AjaxResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <h3>thermal_imaging</h3>
 * <p></p>
 *
 * @author : 李星鹏
 * @date : 2021-01-10 21:45
 **/
@RestController
@RequestMapping("/sys")
@Api("用户")
@Slf4j
public class SubscriberController {

    @Autowired
    private SubscriberService subscriberService;

    @GetMapping("/sub/talk")
    @ApiOperation("机器人对话")
    public AjaxResult talk(String msg){
        String ans=subscriberService.getTalk(msg);
        log.info(ans);
        AjaxResult ajaxResult = new AjaxResult();
        ajaxResult.put("message",ans);
        return ajaxResult;
    }

    @PostMapping("/sub/opinion")
    @ApiOperation("用户反馈")
    public AjaxResult Opinion(String msg, String username){
        subscriberService.sendOpinion(msg,username);
        return AjaxResult.ok("反馈成功");
    }

    @GetMapping("/sub/info")
    @ApiOperation("查看个人体温")
    public AjaxResult Info(String username){
        UserTemp userTemp = subscriberService.getUserTemp(username);
        List<TempAndTime> tempInfo = userTemp.getTempInfo();
        String name = userTemp.getName();
        AjaxResult ajaxResult = new AjaxResult();
        ajaxResult.put("name",name);
        ajaxResult.put("tempInfo",tempInfo);
        return ajaxResult;
    }
}

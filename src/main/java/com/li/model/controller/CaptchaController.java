package com.li.model.controller;

import cn.hutool.core.codec.Base64;
import cn.hutool.core.io.FastByteArrayOutputStream;
import com.google.code.kaptcha.Producer;
import com.li.config.RedisCache;
import com.li.util.Constants;
import com.li.util.AjaxResult;
import com.li.util.UUIDUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

/**
 * <h3>thermal_imaging</h3>
 * <p>验证码操作</p>
 *
 * @author : 李星鹏
 * @date : 2021-01-06 21:28
 **/
@RestController
@Api("验证码")
@Slf4j
public class CaptchaController {
    @Resource(name="captchaProducer")
    private Producer producer;

    @Resource(name="captchaProducerMath")
    private Producer producerMath;

    @Value("${thermal.captchaType}")
    private String captchaType;

    @Autowired
    private RedisCache redisCache;

    @GetMapping("/captchaImage")
    @ApiOperation("验证码接口")
    public AjaxResult getCode(){
        log.info("调用验证码接口");
        String uuid= UUIDUtil.randUUID();
        String verifyKey =Constants.CAPTCHA_CODE_KEY+uuid;

        String capStr = null, code = null;
        BufferedImage image = null;

        if("math".equals(captchaType)){
            String capText=producerMath.createText();
            capStr=capText.substring(0,capText.lastIndexOf("@"));
            code=capText.substring(capText.lastIndexOf("@")+1);
            image=producerMath.createImage(capStr);
        }
        else if("char".equals(captchaType)){
            capStr=code=producer.createText();
            image=producer.createImage(capStr);
        }

        log.info(code);

        redisCache.setCacheObject(verifyKey,code,Constants.CAPTCHA_EXPIRATION, TimeUnit.MINUTES);


        log.info(redisCache.getCacheObject(verifyKey));

        FastByteArrayOutputStream os = new FastByteArrayOutputStream();

        try {
            ImageIO.write(image,"jpg",os);
        } catch (IOException e) {
            return AjaxResult.error(e.getMessage());
        }
        AjaxResult ok = AjaxResult.ok();
        ok.put("uuid",uuid);
        ok.put("img", Base64.encode(os.toByteArray()));
        return ok;
    }

}

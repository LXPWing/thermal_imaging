package com.li.model.vo;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

/**
 * <h3>thermal_imaging</h3>
 * <p>用户登入</p>
 *
 * @author : 李星鹏
 * @date : 2020-12-15 19:46
 **/
@Data
public class LoginUserVO {

    @NotNull(message = "用户名不能为空")
    private String username;

    @NotNull(message = "密码不能为空")
    private String password;

    @NotNull(message = "验证码不能为空")
    private String code;

    private String uuid;
}

package com.li.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

/**
 * <h3>thermal_imaging</h3>
 * <p></p>
 *
 * @author : 李星鹏
 * @date : 2021-05-07 17:09
 **/
@Data
@AllArgsConstructor
public class Permission implements Serializable {
    private Integer id;
    private String name;
    private String value;
    private Integer permission_code;
}

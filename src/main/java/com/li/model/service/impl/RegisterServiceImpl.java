package com.li.model.service.impl;

import cn.hutool.core.codec.Base64Decoder;
import com.arcsoft.face.toolkit.ImageFactory;
import com.arcsoft.face.toolkit.ImageInfo;
import com.li.model.dto.User;
import com.li.model.mapper.UserDao;
import com.li.model.dto.UserFaceInfo;
import com.li.model.service.FaceEngineService;
import com.li.model.service.RegisterService;
import com.li.util.FileNameUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.LinkedHashMap;

/**
 * <h3>thermal_imaging</h3>
 * <p></p>
 *
 * @author : 李星鹏
 * @date : 2020-12-16 18:32
 **/
@Service
@Slf4j
public class RegisterServiceImpl implements RegisterService {

    @Autowired
    private FaceEngineService faceEngineService;

    @Autowired
    private UserDao userDao;
    private String path="C:\\ABC\\";

    @Override
    public void register(User user) {
        byte[] bytes=null;
        UserFaceInfo userFaceInfo = new UserFaceInfo();
        String faceString;
        String fileName;
        LinkedHashMap<Object, Object> map = new LinkedHashMap<>();

        map.put("username",user.getUsername());
        map.put("password",user.getPassword());
        map.put("name",user.getName());
        //map.put("age",user.getAge());
        map.put("sex",user.getSex());
        //map.put("email",user.getEmail());
        map.put("number",user.getNumber());
        //map.put("country",user.getCountry());
        map.put("address",user.getAddress());
        //map.put("face","123");

        //base64还原
        String face=user.getFace();
        bytes=Base64Decoder.decode(face);
        faceString=faceRepresentation(bytes).toString();
        fileName=newFileNmae();

        savegFace(path,bytes,fileName);

        userFaceInfo.setName(user.getName());
        userFaceInfo.setFace_id(faceString);
        
        userDao.saveFaceId(userFaceInfo);
        userDao.saveUser(map);
    }

    /**
     * 人脸特征提取
     * @param file
     * @return
     */
    private byte[] faceRepresentation(byte[] file){
        log.info("人脸特征提取");
        byte[] bytes=null;
        ImageInfo faceData = ImageFactory.getRGBData(file);
        try {
           bytes = faceEngineService.extractFaceFeature(faceData);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return bytes;
    }

    /**
     * 修改上传文件路径
     * @return
     */
    private String newFileNmae(){
       return FileNameUtil.getFileName();
    }

    /**
     * 服务器存储图片
     * @param facePath 文件路径
     * @param bytes 图片
     */
    private void savegFace(String facePath,byte[] bytes,String fileName){
        ByteArrayInputStream byteArrayInputStream=null;
        BufferedImage bufferedImage=null;
        BufferedOutputStream outputStreamFace = null;
        File file=null;
        log.info("调用图片存储");
        try {
            file=new File(facePath);
            byteArrayInputStream=new ByteArrayInputStream(bytes);
            bufferedImage= ImageIO.read(byteArrayInputStream);
            ImageIO.write(bufferedImage,fileName,file);
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            try {
                byteArrayInputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}

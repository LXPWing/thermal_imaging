package com.li.model.service;

import com.li.model.dto.Permission;
import com.li.model.dto.SimpleUser;

import java.util.List;

public interface ShiroService {
    SimpleUser getLoginMG(String username);

    /**
     * 权限集合
     * @param roleid
     * @return
     */
    List<Permission> selectByRoledId(Integer roleid);
}

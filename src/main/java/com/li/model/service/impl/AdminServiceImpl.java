package com.li.model.service.impl;

import com.li.model.vo.UserInfoVO;
import com.li.model.dto.UserOpinion;
import com.li.model.mapper.AdminDao;
import com.li.model.service.AdminService;
import com.li.util.MongoUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <h3>thermal_imaging</h3>
 * <p>\</p>
 *
 * @author : 李星鹏
 * @date : 2021-01-08 09:57
 **/
@Service
public class AdminServiceImpl implements AdminService {
    @Autowired
    private AdminDao adminDao;

    @Autowired
    private MongoUtil mongoUtil;

    @Override
    public List<UserInfoVO> getAllName(Integer page, Integer size) {
        int start=(page-1)*size;
       return adminDao.getAllUserInfo(start, size);
    }

    @Override
    public Long getUserCounts() {
        Long totalCounts = adminDao.getTotalCounts();
        return totalCounts;
    }

    @Override
    public List<UserOpinion> getUserOpinion() {
        UserOpinion userOpinion = new UserOpinion();
        List<UserOpinion> allUserOpi = mongoUtil.findAllUserOpi(userOpinion);
        return allUserOpi;
    }

    @Override
    public void updateOpinionStatus(String id) {
        mongoUtil.updateUserOpi(id);
    }

    @Override
    public void deleteSolved() {
        mongoUtil.deleteStatus();
    }


}

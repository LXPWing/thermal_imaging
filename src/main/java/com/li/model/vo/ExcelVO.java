package com.li.model.vo;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.HeadFontStyle;
import com.li.config.LocalDateTimeConverter;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

/**
 * <h3>thermal_imaging</h3>
 * <p></p>
 *
 * @author : 李星鹏
 * @date : 2021-02-03 21:15
 **/
@Data
@HeadFontStyle(fontHeightInPoints = 9)
public class ExcelVO {
    @ExcelProperty("姓名")
    private String name;

    @ExcelProperty("性别")
    private String sex;

    @ExcelProperty("手机号码")
    private String number;

    @ExcelProperty("地址")
    private String address;

//    @ExcelProperty(value = "时间",converter = LocalDateTimeConverter.class)
//    private List<LocalDateTime> time;

    @ExcelProperty("体温")
    private List<String> temperature;

}

package com.li.config;


import com.li.filer.MyShiroFiler;
import org.apache.shiro.authc.credential.HashedCredentialsMatcher;
import org.apache.shiro.cache.Cache;
import org.apache.shiro.cache.CacheException;
import org.apache.shiro.cache.CacheManager;
import org.apache.shiro.cache.MemoryConstrainedCacheManager;
import org.apache.shiro.mgt.DefaultSecurityManager;
import org.apache.shiro.mgt.RememberMeManager;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.realm.Realm;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.mgt.CookieRememberMeManager;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.apache.shiro.web.servlet.Cookie;
import org.apache.shiro.web.servlet.SimpleCookie;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.processing.Filer;
import java.util.LinkedHashMap;

/**
 * <h3>thermal_imaging</h3>
 * <p></p>
 *
 * @author : 李星鹏
 * @date : 2020-11-28 21:45
 **/

@Configuration
public class ShiroConfiguration {

    /**
     *
     * @param defaultSecurityManager 拦截请求
     * @return
     */
    @Bean
    public ShiroFilterFactoryBean shiroFilterFactoryBean(@Qualifier("defaultSecurityManager") DefaultSecurityManager defaultSecurityManager){
        ShiroFilterFactoryBean shiroFilterFactoryBean=new ShiroFilterFactoryBean();
        shiroFilterFactoryBean.setSecurityManager(defaultSecurityManager);
        LinkedHashMap<String, javax.servlet.Filter> map = new LinkedHashMap<>();
        map.put("jwt",new MyShiroFiler());
        shiroFilterFactoryBean.setFilters(map);
        shiroFilterFactoryBean.setLoginUrl("/sys/login");
        shiroFilterFactoryBean.setUnauthorizedUrl("/sys/login");
        shiroFilterFactoryBean.setFilterChainDefinitionMap(ShiroFilterMapFactory.shiroFilterMap());
        return shiroFilterFactoryBean;
    }

    /**
     *
     * @param shiroRealm 设置安全框架
     * @return
     */
    @Bean("defaultSecurityManager")
    public DefaultWebSecurityManager defaultSecurityManager(@Qualifier("shiroRealm") Realm shiroRealm){
        DefaultWebSecurityManager SecurityManager=new DefaultWebSecurityManager();
        SecurityManager.setRealm(shiroRealm);
        return SecurityManager;
    }



    @Bean("shiroRealm")
    public AuthorizingRealm shiroRealm(){
        ShiroRealm shiroRealm = new ShiroRealm();
        return shiroRealm;
    }

    /**
     * 设置缓存
     * @return
     */
    @Bean
    public CacheManager cacheManager() {
        //使用内存缓存
        return new MemoryConstrainedCacheManager();
    }

    /**
     * 记住我的配置
     * @return
     */
    @Bean
    public RememberMeManager rememberMeManager() {
        Cookie cookie = new SimpleCookie("rememberMe");
        //通过js脚本将无法读取到cookie信息
        cookie.setHttpOnly(true);

        //cookie保存一月
        cookie.setMaxAge(60 * 60 * 24*30);
        CookieRememberMeManager manager=new CookieRememberMeManager();
        manager.setCookie(cookie);
        return manager;
    }

}

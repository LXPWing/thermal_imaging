package com.li.model.service;

import com.li.model.dto.RolePermission;

import java.util.List;

/**
 * <h3>thermal_imaging</h3>
 * <p></p>
 *
 * @author : 李星鹏
 * @date : 2021-05-07 18:24
 **/
public interface RolePermissionService {

    List<RolePermission> selectByRoleId(Integer roleId);

}

package com.li.model.controller;

import com.arcsoft.face.toolkit.ImageFactory;
import com.arcsoft.face.toolkit.ImageInfo;
import com.li.model.service.FaceEngineService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

/**
 * <h3>thermal_imaging</h3>
 * <p></p>
 *
 * @author : 李星鹏
 * @date : 2021-01-24 16:15
 **/
@RestController
@RequestMapping("/sys")
@Api("人脸识别")
@Slf4j
public class FaceController {
    @Autowired
    private FaceEngineService faceEngineService;

    @PostMapping("/face")
    @ApiOperation("人脸识别url")
    public void veriFace(MultipartFile faceFile){
        try {
            byte[] bytes = faceFile.getBytes();
            ImageInfo faceData = ImageFactory.getRGBData(bytes);
            byte[] faceBytes = faceEngineService.extractFaceFeature(faceData);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

}

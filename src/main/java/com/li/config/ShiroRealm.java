package com.li.config;

import com.li.model.dto.Permission;
import com.li.model.dto.SimpleUser;
import com.li.model.service.ShiroService;
import com.li.model.vo.LoginUserVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.stream.Collectors;

/**
 * <h3>thermal_imaging</h3>
 * <p></p>
 *
 * @author : 李星鹏
 * @date : 2020-11-28 20:58
 **/
@Slf4j
public class ShiroRealm extends AuthorizingRealm {

    @Autowired
    private ShiroService shiroService;

    /**
     * 认证
     * @param token
     * @return
     * @throws AuthenticationException
     */
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
        if(token.getPrincipal()==null){
            return null;
        }
        String username=(String)token.getPrincipal();
        log.info("用户:{}正在登录...",username);
        SimpleUser loginMG = shiroService.getLoginMG(username);
        if(loginMG==null){
            throw new UnknownAccountException();
        }else {
           SimpleAuthenticationInfo authenticationInfo=new SimpleAuthenticationInfo(username,loginMG.getPassword(),getName());
           return authenticationInfo;
        }

    }

    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
//        SimpleAuthorizationInfo simpleAuthorizationInfo = new SimpleAuthorizationInfo();
//        SimpleUser user = shiroService.getLoginMG(principals.toString());
//        List<Permission> permissions = shiroService.selectByRoledId(user.getRole());
//        List<String> permissionValues = permissions.stream().map(Permission::getValue).collect(Collectors.toList());
//        simpleAuthorizationInfo.addStringPermissions(permissionValues);
//        return simpleAuthorizationInfo;
        return null;
    }


    /**
     * 清理缓存
     */
    public void clearCachedAuthorizationInfo(){
        this.clearCachedAuthorizationInfo(SecurityUtils.getSubject().getPrincipals());
    }

}

package com.li.model.dto;

import lombok.Data;

import java.util.List;

/**
 * <h3>thermal_imaging</h3>
 * <p>Excel文件</p>
 *
 * @author : 李星鹏
 * @date : 2021-01-08 10:27
 **/
@Data
public class ExcelData {
    private String name;
    private String sex;
    private String number;
    private String address;
    private List<TimeAndTemp> userStatus;
}

package com.li.model.service.impl;

import com.li.model.dto.RolePermission;
import com.li.model.mapper.RolePermissionDao;
import com.li.model.service.RolePermissionService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * <h3>thermal_imaging</h3>
 * <p></p>
 *
 * @author : 李星鹏
 * @date : 2021-05-07 18:25
 **/
public class RolePermissionServiceImpl implements RolePermissionService {

    @Autowired
    private RolePermissionDao rolePermissionDao;

    @Override
    public List<RolePermission> selectByRoleId(Integer roleId) {
        List<RolePermission> rolePermissionById = rolePermissionDao.getRolePermissionById(roleId);
        return rolePermissionById;
    }
}

package com.li.util;

import cn.hutool.http.HttpStatus;

import java.util.HashMap;
import java.util.Map;

/**
 * <h3>thermal_imaging</h3>
 * <p>返回信息</p>
 *
 * @author : 李星鹏
 * @date : 2020-12-16 18:11
 **/
public class AjaxResult extends HashMap<String,Object> {

    public AjaxResult(){
        put("code",0);
        put("msg","success");
    }

    public static AjaxResult error(){
        return error(HttpStatus.HTTP_INTERNAL_ERROR,"未知异常，请联系管理员");
    }

    public static AjaxResult error(String msg){
        return error(HttpStatus.HTTP_INTERNAL_ERROR,msg);
    }

    public static AjaxResult error(int code, String msg){
        AjaxResult ajaxResult =new AjaxResult();
        ajaxResult.put("code",code);
        ajaxResult.put("msg",msg);
        return ajaxResult;
    }

    public static AjaxResult error(Object o){
        AjaxResult ajaxResult=new AjaxResult();
        ajaxResult.put("code",HttpStatus.HTTP_INTERNAL_ERROR);
        ajaxResult.put("data",o);
        return ajaxResult;
    }

    public static AjaxResult error(int code, String msg,Object o){
        AjaxResult ajaxResult =new AjaxResult();
        ajaxResult.put("code",code);
        ajaxResult.put("msg",msg);
        ajaxResult.put("data",o);
        return ajaxResult;
    }

    public static AjaxResult ok(String msg){
        AjaxResult ajaxResult = new AjaxResult();
        ajaxResult.put("msg",msg);
        return ajaxResult;
    }

    public static AjaxResult ok(Map<String,Object> map){
        AjaxResult ajaxResult =new AjaxResult();
        ajaxResult.putAll(map);
        return ajaxResult;
    }

    public static AjaxResult ok(){
        return new AjaxResult();
    }

    public AjaxResult put(String key, Object value){
        super.put(key,value);
        return this;
    }

}

package com.li.util;

import java.util.UUID;

/**
 * <h3>thermal_imaging</h3>
 * <p></p>
 *
 * @author : 李星鹏
 * @date : 2021-01-05 15:33
 **/
public class UUIDUtil {

    public static String getUUID(){
        return UUID.randomUUID().toString().replace("-", "");
    }

    public static String randUUID(){return UUID.randomUUID().toString();}

}

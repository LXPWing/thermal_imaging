package com.li.handle;

import com.li.util.AjaxResult;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authz.AuthorizationException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * <h3>thermal_imaging</h3>
 * <p></p>
 *
 * @author : 梦哟i
 * @date : 2021-05-16 22:55
 **/
@RestControllerAdvice
public class ShiroExceptionHandler {

    @ExceptionHandler(value = AuthenticationException.class)
    public AjaxResult authenticationException(AuthenticationException e){
        return AjaxResult.error("用户名或密码错误");
    }

    @ExceptionHandler(AuthorizationException.class)
    public AjaxResult authorizationException(AuthenticationException e){
        return AjaxResult.error("没有权限");
    }



}

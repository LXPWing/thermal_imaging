package com.li.model.dto;

import com.li.model.dto.TempAndTime;
import lombok.Data;

import java.util.List;

/**
 * <h3>thermal_imaging</h3>
 * <p></p>
 *
 * @author : 李星鹏
 * @date : 2021-01-24 20:32
 **/
@Data
public class UserTemp {
    private String name;
    private List<TempAndTime> tempInfo;
}
